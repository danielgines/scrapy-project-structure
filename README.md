
# Scrapy Project Structure

Esta é a estrutura de diretórios para um Projeto de Spider, que utiliza o Scrapy, SQLAlchemy e Alembic.

Para usar basta executar o start_project.py

```
spider_project/
│
├── alembic/                # Configurations and Alembic migrations
│   ├── versions/           # Generated migration scripts
│   └── env.py
│
├── spider_project/         # Main project module
│   ├── logs/               # Logs files
│   ├── models/             # SQLAlchemy models
│   ├── services/           # Business logic and services
│   ├── spiders/            # Scrapy spiders
│   │   ├── collect/
│   │   ├── monitor/
│   │   └── update/
│   ├── pipelines.py        # Scrapy pipelines (to save to DB)
│   ├── settings.py         # Scrapy configurations
│   └── __init__.py
│
├── .gitignore              # .gitignore rules
├── alembic.ini             # Alembic configurations
├── README.md               # README file
├── requirements.txt        # Project dependencies
└── scrapy.cfg              # Scrapy configurations
```
