import os
import sys

# Verifique se um nome de projeto foi fornecido como argumento
project_name = sys.argv[1] if len(sys.argv) > 1 else "scrapy_project"

# Diretório base
base_dir = project_name

# Subdiretórios
subdirs = [
    "alembic/versions",
    f"{project_name}/logs",
    f"{project_name}/models",
    f"{project_name}/services",
    f"{project_name}/spiders/collect",
    f"{project_name}/spiders/monitor",
    f"{project_name}/spiders/update"
]

# Arquivos a serem criados
files = [
    "alembic/env.py",
    f"{project_name}/pipelines.py",
    f"{project_name}/settings.py",
    f"{project_name}/__init__.py",
    "alembic.ini",
    "requirements.txt",
    "scrapy.cfg"
]

# Criar diretórios
for subdir in subdirs:
    os.makedirs(os.path.join(base_dir, subdir), exist_ok=True)

# Criar arquivos
for file in files:
    open(os.path.join(base_dir, file), 'a').close()

# Copiar .gitignore e README.md para o diretório do projeto
for file_to_copy in [".gitignore", "README.md"]:
    if os.path.exists(file_to_copy):
        with open(file_to_copy, 'rb') as src_file:
            with open(os.path.join(base_dir, file_to_copy), 'wb') as dest_file:
                dest_file.write(src_file.read())
    else:
        print(f"Atenção: {file_to_copy} não existe. Pulando arquivo.")

print("Estrutura de diretórios e arquivos inicializada!")